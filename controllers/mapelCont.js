const { successResponse, errorResponse } = require("../helper/response");
const { Mapel, validateMapel } = require("../models/mapel");

async function get(req, res) {
  const mapel = await Mapel.find();
  res.status(200).json(successResponse("list mapel", mapel));
}

async function add(req, res) {
  const { error } = validateMapel(req.body);
  if (error)
    return res.status(400).json(errorResponse(error.details[0].message));

  let mapel = new Mapel({
    nama: req.body.nama
  });

  await mapel.save();
  res.status(201).json(successResponse("Mapel ditambahkan", mapel));
}

async function update(req, res) {
  const mapel = await Mapel.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true }
  );
  if (!mapel)
    return res.status(404).json(errorResponse("Mapel tidak ditemukan"));

  const { error } = validateMapel(req.body);
  if (error)
    return res.status(400).json(errorResponse(error.details[0].message));

  res.status(200).json(successResponse("Mapel telah diubah", mapel));
}

async function del(req, res) {
  const mapel = await Mapel.findByIdAndDelete(req.params.id);
  if (!mapel)
    return res.status(404).json(errorResponse("Mapel tidak ditemukan"));
  res.status(200).json(successResponse("Mapel berhasil dihapus"));
}

module.exports = { get, add, update, del };
