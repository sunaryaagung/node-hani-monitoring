exports.successResponse = function(message, data) {
  return {
    success: true,
    message: message,
    result: data
  };
};

exports.errorResponse = function(message, err, code) {
  return {
    success: false,
    message: message,
    result: err,
    code: code
  };
};
