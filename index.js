require("express-async-errors");
const express = require("express");
const app = express();
const router = require("./routers");
const cors = require("cors");

//DB
require("./helper/db")();

//EXPRESS
app.use(express.json());
app.use(cors());
app.use("/api", router);
app.get("/", (req, res) => {
  res.json({ message: "API Monitoring guru nurul jadid" });
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`connected to port ${port}`);
});

module.exports = app;
