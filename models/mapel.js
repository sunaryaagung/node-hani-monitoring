const mongoose = require("mongoose");
const Joi = require("@hapi/joi");

const mapelSchema = new mongoose.Schema({
  nama: { type: String, minlength: 1, required: true }
});

const Mapel = new mongoose.model("Mapel", mapelSchema);

//Validator
function validateMapel(mapel) {
  const schema = {
    nama: Joi.string()
      .min(1)
      .required()
  };
  return Joi.validate(mapel, schema);
}

exports.Mapel = Mapel;
exports.validateMapel = validateMapel;
