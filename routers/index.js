const express = require("express");
const router = express.Router();
const mapel = require("./mapelRoute");

router.use("/mapel", mapel);

module.exports = router;
