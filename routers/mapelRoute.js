const express = require("express");
const router = express.Router();
const { get, add, update, del } = require("../controllers/mapelCont");
const validateObjectId = require("../middleware/validateObjectId");

router.get("/", get);
router.post("/", add);
router.put("/:id", validateObjectId, update);
router.delete("/:id", validateObjectId, del);

module.exports = router;
